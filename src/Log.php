<?php


namespace Neneff\Tools;

use Carbon\Carbon;

class Log
{

    // -- type of logs
    static public $LOG_MESSAGE = 'message';
    static public $LOG_WARNING = 'warning';
    static public $LOG_ERROR   = 'error';
    static public $LOG_DEBUG   = '$debug';

    /**
     * @param String $type
     * @param String $message
     * @param array  $data   <p>array of string</p>
     *
     * @return array
     */
    static public function make($type, $message, $data = null)
    {
        $log = [
            'type'    => $type,
            'date'    => Date::getCurrentDateTimeUTC()->format('Y-m-d H:i:s'),
            'message' => $message,
        ];

        if($data) {
            $log['$data'] = $data;
        }

        return $log;
    }

}