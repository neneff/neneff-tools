<?php


namespace Neneff\Tools;


class Variable
{

    /**
     * Return a two dimenstion array with first entry is the original key and the second its original associated value
     * @param array $array
     * @return array
     */
    static public function pairs(array $array)
    {
        $result = [];
        foreach($array as $key => $value)
        {
            $result[] = [$key, $value];
        }
        return $result;
    }

    /**
     * Return the value or null if not isset
     * @param $value
     * @return mixed|null
     */
    static public function issetOrNull(&$value)
    {
        return isset($value) ? $value : null;
        /*
         - Tests :
        $values = ['v1' => 'value 1', 'v2' => 'value 2'];
        $empty  = [];

        var_dump(issetOrNull($notExists));                          => null
        var_dump(issetOrNull($notExists['nowhere']));               => null
        var_dump(issetOrNull($empty));                              => []
        var_dump(issetOrNull($empty['nowhere']));                   => null
        var_dump(issetOrNull($empty['nowhere']['andFurther']));     => null
        var_dump(issetOrNull($values));                             => ['v1' => 'value 1', 'v2' => 'value 2']
        var_dump(issetOrNull($values['nowhere']));                  => null
        var_dump(issetOrNull($values['v1']));                       => 'value 1'
        */
    }

    /**
     * Return the value or 0 if not isset
     * @param $value
     * @return mixed|integer
     */
    static public function issetOrZero(&$value)
    {
        return isset($value) ? $value : 0;
    }

    /**
     * Return the value or false if not isset
     * @param $value
     * @return mixed|false
     */
    static public function issetOrFalse(&$value)
    {
        return isset($value) ? $value : false;
    }

    /**
     * Return the value or empty string if not isset
     * @param $value
     * @return mixed|string
     */
    static public function issetOrEmpty(&$value)
    {
        return isset($value) ? $value : '';
    }

}