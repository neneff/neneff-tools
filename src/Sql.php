<?php


namespace Neneff\Tools;


class Sql
{

    /**
     * @param \PDO $pdo
     * @param $values
     * @param $valueByRow
     * @param \Closure $function  <p>Should return a prepared statement or null</p>
     * @param bool $useTransaction
     * @return \PDOStatement[]
     * @throws \Exception
     */
    static public function simpleChunkedQuery(\PDO $pdo, $values, $valueByRow, \Closure $function, $useTransaction = false)
    {
        $maxEntitiesPerQuery    = 65535; // max allowed for a prepared statement
        $numberOfValuesPerChunk = floor($maxEntitiesPerQuery /$valueByRow) *$valueByRow;
        $stmts                  = [];

        try
        {
            if($useTransaction) {
                $pdo->beginTransaction();
            }

            // -- Query
            foreach(array_chunk($values, $numberOfValuesPerChunk) as $valueChunk)
            {
                $placeholders = self::preparePlaceholdersList(count($valueChunk), $valueByRow);
                $stmt         = $function($pdo, $placeholders);

                if($stmt !== null)
                {
                    if(!($stmt instanceof \PDOStatement)) {
                        throw new \LogicException('simpleChunkedQuery Closure is supposed to return \PDOStatement, "'.gettype($stmt).'" returned.');
                    }

                    /** @var \PDOStatement $stmt */
                    $stmt->execute($valueChunk);
                    $stmts[] = $stmt;
                }
            }

            if($useTransaction) {
                $pdo->commit();
            }
        }
        catch(\Exception $e)
        {
            if($useTransaction) {
                $pdo->rollBack();
            }
            throw $e;
        }

        return $stmts;
    }

    /**
     * @param $valueCount
     * @param $valueByRow
     * @return string | false
     * @throws \Exception
     */
    static public function preparePlaceholdersList($valueCount, $valueByRow)
    {
        // at least One row to be available
        if($valueCount >= $valueByRow)
        {
            $placeholderRow  = implode(',', array_fill(0, $valueByRow, '?'));
            $placeholderRows = implode(',', array_fill(0, $valueCount /$valueByRow, '('.$placeholderRow.')'));

            return $placeholderRows;
        }
        else
        {
            throw new \Exception("Trying to prepare statement without data expected minimum of {$valueByRow}, received {$valueCount} total");
        }
    }

}