<?php

namespace Neneff\Tools;


class Profiler
{
    static protected $PROFILER_ACTIVATED  = true;
    static protected $PROFILER_REGISTERED = [];

    protected $_started     = false;
    protected $_start       = 0;
    protected $_elapsed     = 0;
    protected $_elapsedList = [];

    /**
     *
     */
    public function __construct()
    {
        if (!defined('DEBUG') || !DEBUG) {
            self::$PROFILER_ACTIVATED = false;
        }
        else {
            self::$PROFILER_ACTIVATED = true;
        }
    }

    /**
     * start a session
     */
    public function start()
    {
        if(!self::$PROFILER_ACTIVATED) {
            return;
        }

        if(!$this->_started)
        {
            $this->_start   = microtime(true);
            $this->_started = true;
        }
    }

    /**
     * stop a session and stack to total elapsed
     * @param string $tag
     */
    public function stop($tag='default')
    {
        if(!self::$PROFILER_ACTIVATED) {
            return;
        }

        if($this->_started)
        {
            // new data
            $stop                 = microtime(true);
            $elapsedTime          = $stop -$this->_start;
            // update data
            $this->_elapsed          += $elapsedTime;
            $this->_started           = false;

            if(!isset($this->_elapsedList[$tag])) {
                $this->_elapsedList[$tag] = [];
            }

            $this->_elapsedList[$tag][] = [
                'start'   => $this->_start,
                'stop'    => $stop,
                'elapsed' => $elapsedTime
            ];
        }
    }

    /**
     * @return float
     */
    public function getElapsedTime()
    {
        return $this->_elapsed;
    }

    /**
     * @return array
     */
    public function getElapsedList()
    {
        return $this->_elapsedList;
    }

    /**
     * @return bool
     */
    public function isActivated()
    {
        return self::$PROFILER_ACTIVATED;
    }

    /**
     * @param Profiler $profiler
     * @param string      $key [optional]
     */
    static public function registerProfiler(Profiler $profiler, $key='')
    {
        if(!self::$PROFILER_ACTIVATED) {
            return;
        }

        if(!isset(self::$PROFILER_REGISTERED[$key]))
        {
            self::$PROFILER_REGISTERED[$key] = [];
        }
        self::$PROFILER_REGISTERED[$key][] = $profiler;
    }

    /**
     * @param  string  $key
     * @return float
     */
    static public function sum($key='')
    {
        if(isset(self::$PROFILER_REGISTERED[$key]) && count(self::$PROFILER_REGISTERED[$key]) > 0)
        {
            $total = 0;
            foreach(self::$PROFILER_REGISTERED[$key] as $profiler) {
                $total += $profiler->getElapsedTime();
            }
            return  $total;
        }
        else
        {
            return null;
        }
    }

    /**
     * @param  string  $key
     * @return float
     */
    static public function average($key='')
    {
        if(isset(self::$PROFILER_REGISTERED[$key]) && count(self::$PROFILER_REGISTERED[$key]) > 0)
        {
            $count = count(self::$PROFILER_REGISTERED[$key]);
            $total = 0;
            foreach(self::$PROFILER_REGISTERED[$key] as $profiler) {
                $total += $profiler->getElapsedTime();
            }
            return  $total/$count;
        }
        else
        {
            return null;
        }
    }

    /**
     * @param  string  $key
     * @return integer
     */
    static public function count($key='')
    {
        if(isset(self::$PROFILER_REGISTERED[$key])) {
            return count(self::$PROFILER_REGISTERED[$key]);
        }
        else {
            return 0;
        }
    }

    /**
     * @return Array
     */
    static public function keys()
    {
        return array_keys(self::$PROFILER_REGISTERED);
    }


    /**
     * @param  integer $bytes
     * @return string
     */
    static public function byteToMbyte($bytes)
    {
        return (floor($bytes *100 /1024 /1024) /100).' Mb';
    }

}