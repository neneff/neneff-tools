<?php


namespace Neneff\Tools;

use Carbon\Carbon;

class Date
{

    /**
     * Get Current DateTime UTC
     * @return Carbon
     */
    static public function getCurrentDateTimeUTC()
    {
        $datetime = Carbon::now('UTC');
        return $datetime;
    }


    /**
     * @param  String $format
     * @return String
     */
    static public function getCurrentTimestamp($format='Y-m-d H:i:s')
    {
        //        $the_date = strtotime(gmdate("M d Y H:i:s"));
        //        echo(date_default_timezone_get() . "<br />");
        //        echo(date("Y-d-mTG:i:sz",$the_date) . "<br />");
        //        echo(date_default_timezone_set("UTC") . "<br />");
        //        echo(date("Y-d-mTG:i:sz", $the_date) . "<br />");
        //
        //        echo strtotime(gmdate("M d Y H:i:s")) . "<br />";
        //        echo gmdate("Y-m-d\TH:i:s\Z") . "<br />";

        $dateTime = new \DateTime('NOW');
        $dateTime->setTimezone(new \DateTimeZone('UTC'));
        return $dateTime->format($format);
    }

}